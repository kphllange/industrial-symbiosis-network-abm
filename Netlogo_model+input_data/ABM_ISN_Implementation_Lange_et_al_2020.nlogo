; Wastesuppliers and Wasteprocessors (social agents) are breeds of turtle
; Incinerators (nonsocial agents) are breeds of turtle
; 1 tick represents 1 season
; ISN consists of companies within 5 km, environment > 5 km


;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2 do: verify transactions and evaluations. Add NPV..

; set globals
extensions
[
  csv                                    ; activates Netlogo csv extension
  matrix                                 ; activates Netlogo matrix extension
]

globals
[
  PeriodID                                                 ; [int >=0, ID of the Period] equal to tickID
  YearCounter                                              ; [int >=0, number of years] year
  SeasonID                                                 ; [int 1<=x<=4, ID of the Season], four seasons
  ;Wprice                                                  ; [euro/ton] standard market price for WSSeason if regarded as waste
  ;BPPrice                                                 ; [euro/ton] standard market price for WSSeason if regarded as by-product
  ;Cprice                                                  ; [euro/ton] standard market price for compost
  ;Waste-as-BP?                                            ; [boolean], determines if WSSeason is regarded as waste or by-product
  Wavail                                                   ; WSSeason avaliability on the market (% of the yearly availibility)
  TotWSWYear                                               ; [tons] total amount of waste of all suppliers in the network per year
  TotWSWSeason                                             ; [tons] total amount of waste of all suppliers in the network in the current season
  QuarterlyWasteMat                                        ; matrix with available waste per quarter per supplier, from (external) datafile
  SN-patches                                               ; patches that represent the symbiotic network environment
  C-patches                                                ; patches that represent the surrounding environment (context)
  SNFailure                                                ; [boolean], if true, Symbiotic Network (SN) has failed

  ; for behaviorspace
  ;myGlobalWSB
  ;myGlobalWSMoney
  WastesupplierIDList

  initWSB1
  initWSB2
  initWSB3
  initWSB4
  initWSB5
  initWSB6
  initWSB7
  initWSB8
  initWSB9
  initWSB10
  initWSB11
  initWSB12
  initWSB13
  initWSB14
  initWSB15
  initWSB16

  WSB1
  WSB2
  WSB3
  WSB4
  WSB5
  WSB6
  WSB7
  WSB8
  WSB9
  WSB10
  WSB11
  WSB12
  WSB13
  WSB14
  WSB15
  WSB16

  WSMoney1
  WSMoney2
  WSMoney3
  WSMoney4
  WSMoney5
  WSMoney6
  WSMoney7
  WSMoney8
  WSMoney9
  WSMoney10
  WSMoney11
  WSMoney12
  WSMoney13
  WSMoney14
  WSMoney15
  WSMoney16

  WSMoneyPerUnit1
  WSMoneyPerUnit2
  WSMoneyPerUnit3
  WSMoneyPerUnit4
  WSMoneyPerUnit5
  WSMoneyPerUnit6
  WSMoneyPerUnit7
  WSMoneyPerUnit8
  WSMoneyPerUnit9
  WSMoneyPerUnit10
  WSMoneyPerUnit11
  WSMoneyPerUnit12
  WSMoneyPerUnit13
  WSMoneyPerUnit14
  WSMoneyPerUnit15
  WSMoneyPerUnit16

  CumuWS2WPTransact1
  CumuWS2WPTransact2
  CumuWS2WPTransact3
  CumuWS2WPTransact4
  CumuWS2WPTransact5
  CumuWS2WPTransact6
  CumuWS2WPTransact7
  CumuWS2WPTransact8
  CumuWS2WPTransact9
  CumuWS2WPTransact10
  CumuWS2WPTransact11
  CumuWS2WPTransact12
  CumuWS2WPTransact13
  CumuWS2WPTransact14
  CumuWS2WPTransact15
  CumuWS2WPTransact16

  ;WSWTotal1
  ;WSWTotal2
  ;WSWTotal3
  ;WSWTotal4
  ;WSWTotal5
  ;WSWTotal6
  ;WSWTotal7
  ;WSWTotal8
  ;WSWTotal9
  ;WSWTotal10
  ;WSWTotal11
  ;WSWTotal12
  ;WSWTotal13
  ;WSWTotal14
  ;WSWTotal15
  ;WSWTotal16

  initWPB0
  WPB0
  WPMoneyPerUnit0
  WPMoney0
  CumuWP2CBTransact0

  MeanContractPrices
]

; breed social agents
breed [Wastesuppliers Wastesupplier]
breed [Wasteprocessors Wasteprocessor]
breed [Incinerators Incinerator]

; breed technical agents (objects)
breed [CBs CB]                                             ; Compost Batches
undirected-link-breed [contracts contract]
;undirected-link-breed [TotTransferlinks TotTransferlink]

; agents own variables

Wastesuppliers-own
[
  ;; Agent inputs
  WSID                                                     ; [int >= 0]; identification number
  WSWIfixedContractAge                                     ; [int >=0,int]; time left until existing fixed contract with WI breaks open. Until that time, WS can't participate in symbiosis
                                                           ; ASSUMPTION: after breaking open the fixed contract, WS never takes a fixed contract again with WI.

  ;; Waste parameters
  WSWYear                                                  ; [float >=0, tons]; total amount of waste per supplier per year
  WSWSeason                                                ; [float >=0, tons]; amount of waste per supplier per season
  WSWqual                                                  ; [float 0<=x<=1]; between 0 and WSWQualRNorm = useless, WSWQualRNorm = just usable, 1 highest quality
  WSWvalue                                                 ; [float, euro/ton]; economic value of the WSWSeason in scenario waste_as_waste, without taking quality into account
  WSBPvalue                                                ; [float, euro/ton]; economic value of the WSWSeason in scenario waste_as_by-product, without taking quality into account
  Wpriceq                                                  ; [float, euro/ton]; economic value of WSWValue or WSBPValue (depending on scenario), taking quality into account

  ;; Negotiation parameters
  WSOffercounter                                           ; [int, >=0]; counts the amount of offers made in one round
  WSMaxOfferCounter                                        ; [int, >=0]; max. amount of offers in one negotiation round
  WSWvaluelimit                                            ; [float, euro/ton]; limit value of the waste (perception)
  WSWvaluetarget                                           ; [float, euro/ton]; target value of waste (perception)
  WSOffer                                                  ; [float, euro/ton]; offer by supplier
  WSlastWSOffer                                            ; [float, euro/ton]; last WSOffer in bidding round
  WSlastWPOffer                                            ; [float, euro/ton]; last WSOffer in bidding round

  ;; Behavioural parameters TPB
  initWSB
  WSPBC                                                    ; [float 0<=x<=1]; perceived behavioral control of the supplier
  WSWFPBC                                                  ; [int, 0<=x<=7]; weight factor regarding perceived behavioral control of the supplier
  WSEPBC                                                   ; [float 0<=x<=0.2]; Evaluation ratio perceived behavioral control of supplier
  WSAtt                                                    ; [float 0<=x<=1]; attitude towards symbiosis
  WSWFAtt                                                  ; [int, 0<=x<=7]; weight factor regarding suppliers' attitude towards symbiosis
  WSEAtt                                                   ; [float 0<=x<=0.2]; Evaluation ratio attitude of supplier
  WSSN                                                     ; [float 0<=x<=1]; Subjective Norm regarding engaging in symbiosis
  WSWFSN                                                   ; [int, 0<=x<=7]; weight factor regarding Subjective Norm of suppliers towards symbiosis
  WSESN                                                    ; [float 0<=x<=0.2]; Evaluation ratio subjective norm of supplier
  WSBI                                                     ; [float 0<=x<=1]; total supplier behavioural intention to engage in symbiosis
  WSBIEvaluated?                                           ; [boolean]; if true the wastesupplier already evaluated its behaviour in this tick
  WSB                                                      ; [float 0<=x<=1]; total supplier actual behaviour to engage in symbiosis

  ;; Outputs
  WS2WPTransact                                            ; [float >=0, ton]; amount of waste to be transacted to WP during this tick
  WS2WITransact                                            ; [float >=0, ton]; amount of waste to be transacted to WI during this tick
  WSOffer                                                  ; [float >=0, euro/ton]; offer by supplier
  WSOffering?                                              ; [boolean]; if true, WS will do an offer during this tick
  WSContract?                                              ; [boolean]; shows if WS has a contract with WP
  WSContractDeli?                                          ; [boolean]; shows if WS has delivered according to contract this round
  WSmoney                                                  ; [float >=0, euro]; cashflow as a result of symbiotic activities
  WSOldmoney                                               ; [float >=0, euro]; cashflow as a result of symbiotic activities, measured before transactions this round
  WSNewmoney                                               ; [float >=0, euro]; cashflow as a result of symbiotic activities, measured after transactions this round
  Deal?                                                    ; [boolean]; if true, WS has made a deal this round
  CumuWS2WPTransact                                        ; [float >=0, ton]; cumulative amount of waste transacted from WS to WPs
  WSMoneyPerUnit                                           ; [float, euro/ton]; amount of money per ton earned (cumulative over time)
]

Wasteprocessors-own
[
  ;; Inputs
  WPID                                                     ; [int >=0]; ID number for WP
  WPProc                                                   ; [float >=0, ton]; amount of waste in the processor at a moment in time
  WPMaxProc                                                ; [float >=0, ton]; maximum amount of waste in the processor at a moment in time
  WPMaxCapLeft                                             ; [float >=0, ton]; Max Capacity Left at a moment in time
  WPI/Oratio                                               ; [float 0<=x<=1]; Input-output ratio from technology used by WP

  ;; Decision making parameters
  WPEnoughThreshold                                        ; [float 0<=x<=1]; threshold for deciding whether WP is full enough to have a positive effect on WPPBC
  WPEnoughThreshold?                                       ; [boolean]; for deciding to try collecting waste (WPEnoughThreshold = False) or not (WPEnoughThreshold = True) from either contracted or noncontracted suppliers
  WPEnough100?                                             ; [boolean]; for deciding to go on transacting waste (WPEnough100=False) or to stop transacting waste (WPEnough100=True)
  WPQualThreshold                                          ; [float 0<=x<=1]; threshold for deciding whether the waste quality if high enough

  ;; Negotiation parameters
  WPvaluelimit                                             ; [euro/ton]; limit value for WP in this bidding round
  WPvaluetarget                                            ; [euro/ton]; target value for WP in this bidding round
  WPAccepted?                                              ; [boolean]; if true, WP accepts this WSoffer

  ;; Behavioural parameters
  initWPB
  WPPBC                                                    ; [float 0<=x<=1]; perceived behavioral control of the processor
  WPWFPBC                                                  ; [int, 0<=x<=7]; weight factor regarding perceived behavioral control of the processor
  WPEPBC                                                   ; [float 0<=x<=0.2]; Evaluation ratio perceived behavioral control of processor
  WPAtt                                                    ; [float 0<=x<=1]; processor attitude towards symbiosis
  WPWFAtt                                                  ; [int, 0<=x<=7]; weight factor regarding attitude of Wasteprocessor towards symbiosis
  WPEAtt                                                   ; [float 0<=x<=0.2]; Evaluation ratio attitude of processor
  WPSN                                                     ; [float 0<=x<=1]; Subjective Norm regarding engaging in symbiosis
  WPWFSN                                                   ; [int, 0<=x<=7]; weight factor regarding Subjective Norm of Wasteprocessor towards symbiosis
  WPESN                                                    ; [float 0<=x<=0.2]; Evaluation ratio subjective norm of wasteprocessor
  WPBI                                                     ; [float 0<=x<=1]; total processor behavourial intention to engage in symbiosis
  WPBIEvaluated?                                           ; [boolean]; if true the wasteprocessor already evaluated its behaviour in this tick
  WPB                                                      ; [float 0<=x<=1]; actual behaviour value of WP to engage in symbiosis

  ;; Outputs
  WPoffer                                                  ; [float, euro/ton], offer (euro/ton)
  WPmoney                                                  ; [float >=0, euro]; cashflow as a result of symbiotic activities
  WPemptycounter                                           ; [int >=0]; amount of seasons that WP is not used
  WPOldmoney                                               ; [float >=0, euro]; cashflow as a result of symbiotic activities, measured before transactions this round
  WPNewmoney                                               ; [float >=0, euro]; cashflow as a result of symbiotic activities, measured after transactions this round
  CumuWP2CBTransact                                        ; [float >=0, ton]; cumulative amount of waste transacted from WP to CPs
  WPMoneyPerUnit                                           ; [float, euro/ton]; amount of money per ton waste earned (cumulative over time)

  ;; object related
  CB-Counter                                               ; [int >=0]; counts the amount of compost heaps (batches)
  ContractCounter                                          ; [int >=0]; counts amount of contracts with waste suppliers

  ;; Lists
  ContractedWSIDsList                                      ; [agentset list] of all current Contracted WSIDs
  WPSucCSList                                              ; [agentset list] of current Contracted WSIDs that are able to deliver waste with a high enough quality
  CurrentCBIDsList                                         ; [agentset list] of all current CBIDs
  HistoricalCBIDsList                                      ; [agentset list] of all CBIDs in the past

  ;; Agentsets
  WPSuccessfulContractSuppliers                            ; [agentset] of  ContractedWSIDs that actually can deliver according to the contract
]

Incinerators-own
[
  ;;Incinerator parameters
  WIID                                                     ; [int >=0]; ID number for WI
  WIprice                                                  ; [float >=0, euro/ton]; price of incinerating waste that is dumped to WI
  WIAmountIncinerated                                      ; [float >=0, ton]; total amount of incinerated waste
  WIMoney                                                  ; [float >=0, euro]; cashflow of incinerator (as a result of nonsymbiotic activities)
]

CBs-own
[
  CBID                                                     ; [int >=0]; ID number for compost batch (CB)
  OwnedbyWPID                                              ; [int >=0]; ID number of WP that owns this CB
  SetupPeriodID                                            ; [int >=0]; ID number of the Period in which this batch was setup
  initCBMass                                               ; [float >0, ton]; initial mass of CB
  CBMass                                                   ; [float >0, ton]; actual mass of CB
  CBAge                                                    ; [int >=0, amount of periods]; age of CB
  CBRtime                                                  ; [int >=0, amound of periods]; retention time for composting process
  CBReady?                                                 ; [boolean]; if true, the CB is ripe and ready to be sold
]

Contracts-own
[
  ContractID                                               ; [int >=0]; ID number for contract
  ContractAgreedPrice                                      ; [float, euro/ton]; agreed price per ton waste
  ContractAgreedMass                                       ; [float, euro/ton]; agreed maximum mass that will be transacted in a period according to the contract
  ContractLength                                           ; [int >=0, amount of periods]; max. duration of the contract
  ContractAge                                              ; [int >=0, amount of periods]; actual duration of the contract
  WSPartnerID                                              ; [int >=0, WSID number], of WS who agreed to this contract
  WPPartnerID                                              ; [int >=0, WPID number], of WP who agreed to this contract
]

__includes
[
  "SetupSN.nls"                                            ;Flowchart A
  "SetupWPBI.nls"                                          ;Flowchart B
  "SetupWSBI.nls"                                          ;Flowchart C
  "FetchWasteAvailCSV.nls"                                 ;Flowchart D
  "WSWvalueCalc.nls"                                       ;Flowchart E
                                                           ;Flowchart F n.a.
  "WPAskContractedWSIDs.nls"                               ;Flowchart G

  "WPAskNonContractedWSIDs.nls"                            ;Flowchart H
  "SNNegotiation.nls"                                      ;Flowchart I
  "WSStartsNegotiation.nls"                                ;Flowchart J
  "WPStartsNegotiation.nls"                                ;Flowchart K
  "WPFindPotentialWSs.nls"                                 ;Flowchart L
  "WSSetOfferV2.nls"                                       ;Flowchart M
                                                           ;Flowchart N n.a.
                                                           ;Flowchart O n.a.
  "WSTransact.nls"                                         ;Flowchart P
  "WSDumpWaste.nls"                                        ;Flowchart Q
  "WSEvaluate.nls"                                         ;Flowchart R and AG
  "WSUpdateLinks.nls"                                      ;Flowchart S
  "WPSellCompost.nls"                                      ;Flowchart T
  "WPSetOfferV2.nls"                                       ;Flowchart U
  "WPAcceptWaste.nls"                                      ;Flowchart W
  "WPCompareContracts.nls"                                 ;Flowchart X
  "WPEstablishContract.nls"                                ;Flowchart Y
  "WPEvaluate.nls"                                         ;Flowchart Z and AF
  "WPupdateCB.nls"                                         ;Flowchart AA
  "WPFillCB.nls"                                           ;Flowchart AB
  "WIAcceptDumpedWaste.nls"                                ;Flowchart AC
  "WPCapacityCheck.nls"                                    ;Flowchart AD
  "CalculateBI.nls"                                        ;Flowchart AE
  ;WPLiveordie                                             ;Flowchart AF, see Z WPEvaluate
  ;WSLiveordie                                             ;Flowchart AG, see R WSEvaluate
  "NotSetupWPBI.nls"                                       ;Flowchart AH
  "NotSetupWSBI.nls"                                       ;Flowchart AI
  ;ForBehaviorSpace                                        ;Flowchart AJ
  "WSMakeGlobal.nls"                                       ;Flowchart AK
  "WPMakeGlobal.nls"                                       ;Flowchart AL

]

;-----------------------------------------------------------------------------------------------------------------------------------------------;

to setup                                                                    ;; Flowchart 01 Setup: this happens when pushing the 'setup' button
if CheckPath? [
    output-print "01 Setup"
  ]


  SetupSN                                                                       ; Flowchart A: setup symbiotic network, creation of agents, patches etc.
                                                                                ;[CBID] of CBs                                                              ; use IDs of existing compostbatches


  ifelse TPB? = true
  [
    SetupWPBI                                                                 ; FLowchart B: setup BI and B for waste processors

    SetupWSBI                                                                 ; Flowchart C: setup BI and B for waste suppliers
  ]
  [
    NotSetupWPBI                                                              ; Flowchart AH: TPB remains neutral

    NotSetupWSBI                                                              ; Flowchart AI: TPB remains neutral
  ]



  update-plots

end
;-----------------------------------------------------------------------------------------------------------------------------------------------;
to go-once                                                                  ;; FLOWCHART 02. this happens when pushing the 'go-once' button

  if CheckPath? [
    output-print "02 Go-Once"
  ]


  if PeriodID >= 40 [ stop ]                                                  ;; stop after x seasons

  set PeriodID ticks + 1

  let countWS count Wastesuppliers
  let countWP count Wasteprocessors

  ;if countWS = 0 OR countWP = 0
  ;[
  ;ask patch 0 0 [set plabel "Symbiotic Network Failed"]
  ;set SNFailure True
  ;set PeriodID 40
  ;stop
  ;]

  Ifelse SeasonID >= 5                                                      ; determine which season it is: 1=winter, 2=spring, 3=summer, 4=autumn
  [
    set SeasonID 1
  ]
  [
    set SeasonID SeasonID + 1
  ]

  ask patch -10 20 [set plabel "Period"]                                    ; show periodID
  ask patch -10 19 [set plabel PeriodID]
  ask patch 10 20 [set plabel "Season"]                                     ; show seasonID
  ask patch 10 19 [set plabel SeasonID]

  Ask Wastesuppliers                                                       ;; check for waste availibility, using csv and SeasonID
  [
    ;; reset Wastesupplier parameters in new season

    set WSBIEvaluated? false
    set WS2WPTransact 0
    set WS2WITransact 0

    set WSOffering? false                                                   ; DEFENSIVE PROGRAMMING: before new bidding round, no one is currently offering

    let my-WasteCSVlist WSFetchAvailableWaste                              ;; Flowchart D WSFetchWasteAvailCSV: report waste per season per supplier
    WSID                                                                    ; Wastesupplier ID
    SeasonID                                                                ; number, Quarter 1 winter to 4 autumn
    QuarterlyWasteMat                                                       ; matrixID
    WSContract?                                                             ; [boolean]
    set WSWSeason item 0 my-WasteCSVlist                                    ; [tons/season]
    set WSWyear item 1 my-WasteCSVlist                                      ; [tons/year]
    set TotWSWSeason item 2 my-WasteCSVlist                                 ; [tons]
    set TotWSWYear item 3 my-WasteCSVlist                                   ; [tons]
    set WSWQual item 4 my-WasteCSVlist                                      ; [float], quality expressed from 0 (poor) to 1 (high)



    set label precision WSWSeason 1


    if CheckFetch?
    [
      output-print word WSID WSWSeason
    ]


    if Waste_as_by-product? = true                                          ; ASSUMPTION: QUALITY IS 20% HIGHER WHEN WASTE IS TREATED AS BY-PRODUCT
    [
      set WSWQual WSWQual + 0.2
      if WSWQual > 1
      [
        set WSWQual 1
      ]
    ]
    ; if Waste_as_by-product? = false: nothing happens, the quality remains the same

    let my-WSWValueList WSWvalueCalc                                        ;; Flowchart E: report waste avalailibility and Value per supplier
    WSID                                                                ; Wastesupplier ID
    WSWSeason                                                           ; [tons/season]
    WSWyear                                                             ; [tons/year]
    Wprice                                                              ; [euro/ton], used when waste is treated as waste
    BPprice                                                             ; [euro/ton], used when waste is treated as by-product
    WSWqual                                                             ; [Netlogo value: 0 to 1, useless to highly valuable]
    TotWSWSeason
    TotWSWYear
    Waste_as_by-product?
    set Wavail item 0 my-WSWValueList                                     ; [tons]
    set Wpriceq item 1 my-WSWValueList                                    ; [euro/ton]
    set WSWvalue item 2 my-WSWValueList                                   ; [euro/ton]
    set WSBPvalue item 3 my-WSWValueList                                   ; [euro/ton]
                                                                           ;show (list who Wvalue)
  ]

  ;show TotSeason
  ;show Totyear

  update-plots


  Ask wasteprocessors                                                      ;;
  [
    set WPBIEvaluated? false

    let my-WPID [WPID] of self
    let my-CBs CBs with [OwnedbyWPID = my-WPID]
    set CurrentCBIDsList [CBID] of my-CBs
    ;print CurrentCBIDsList

    WPupdateCB                                                            ;Flowchart AA: WPUpdateCB
    CurrentCBIDslist

    let CWSs Wastesuppliers with [WSContract? = true]
    set ContractedWSIDsList []
    if CWSs != nobody
    [
      set ContractedWSIDsList [WSID] of CWSs
    ]

    let SoldCBIDslist WPSellCompost                                      ;; Flowchart T: WPSellCompost: sell compost if a CB is ready (CBAge = CBRtime)
    CurrentCBIDsList
    HistoricalCBIDsList
    WPID
    WPMoney
    WPProc
    WPMaxProc
    WPMaxCapLeft
    CPrice

    set CurrentCBIDsList item 0 SoldCBIDslist
    set HistoricalCBIDsList item 1 SoldCBIDslist
    set WPMoney item 2 SoldCBIDslist
    set WPProc item 3 SoldCBIDslist
    set WPMaxCapLeft item 4 SoldCBIDslist

    ;print CurrentCBIDsList
    ;print HistoricalCBIDsList
    ;show WPMoney
    ;show WPProc
    ;show WPMaxCapLeft

    let WPCap95List WPCapacityCheck                                        ;; Flowchart AD: ASSUMPTION: make decision to try to collect waste based on fill rate until threshold
    WPProc
    WPMaxProc                                                            ;; ASSUMPTION:	The waste processor never exceeds the maximum allowed capacity of 480 tons (100%) of organic matter at a specific point in time.
    WPProcThresholdPerc                                                  ;; [float], threshold of max capacity that is necessary to make a decision (value 0-1)

    set WPEnoughThreshold? item 0 WPCap95List
    ;print WPProc
    ;print WPEnough95?

    ifelse WPEnoughThreshold? = false
    [
      WPAskContractedWSIDs                                                ;; Flowchart G, ASSUMPTION: first ask contracted suppliers (only if there is enough processing capacity left)
                                  QuarterlyWasteMat
    ]
    [
            WPEvaluate                                                         ;; Flowchart Z: WPEvaluate
    ]

  ]

  update-plots                                                             ;; update plots to see how much waste is in processor after selling compost

  if any? Wasteprocessors with [WPenoughThreshold? = false]                ;; If all wasteprocessors are full, go to the next season
  [
    let NonEvalWP Wasteprocessors with [WPBIEvaluated? = false]
    Ask NonEvalWP
    [
      let WPCap95List WPCapacityCheck                                      ;; Flowchart AD, ASSUMPTION: make decision to try to collect waste based on fill rate until threshold
      WPProc
      WPMaxProc
      WPProcThresholdPerc                                                  ;; [float], threshold of max capacity that is necessary to make a decision (value 0-1)

      set WPEnoughThreshold? item 0 WPCap95List
      ;print WPProc
      ;print WPEnough95?

      ifelse WPEnoughThreshold? = false
      [
        WPAskNonContractedWSIDs                                            ;; Flowchart H: ASSUMPTION, if WP not full after contracted suppliers, ask noncontracted suppliers
        QuarterlyWasteMat
      ]                                                                    ;; end of ifelse WPProc < 95 %

      [
        WPEvaluate                                                         ;; Flowchart Z: WPEvaluate
      ]

    ]
  ]

  update-plots

  let NonSymbioticSuppliers Wastesuppliers with
  [
    WSWSeason > 0                                                            ; define rest of Wastesuppliers (with still some waste)
  ]

  ask NonSymbioticSuppliers                                                  ;
    [
      let WSAgents self                                                      ; define relevant Wastesupplier
      let WIAgent one-of Incinerators                                        ; define Incinerator

      let my-WSlist4 WSWPDumpWaste                                          ;; Flowchart Q: report transaction of by-products

                 WSAgents                                                               ; inputs from Wastesuppliers
                 WSWSeason

                 WIAgent                                                                ; inputs from Incinerator
                 [WIAmountIncinerated] of WIagent
                 [WIprice] of WIagent
                 [WIMoney] of WIagent

                 ;; output dump waste
                 set WSWSeason item 0 my-WSlist4                                        ; Wastesupplier variables

  ]

  update-plots

  if any?
  Wastesuppliers with [WSBIEvaluated?] = false
  OR
  Wasteprocessors with [WPBIEvaluated?] = false

  [
    let NonEvalWS Wastesuppliers with [WSBIEvaluated? = false]

    Ask NonEvalWS
    [
      let my-WS self

      WSEvaluate                                                           ;; Flowchart R: WSEvaluate
      my-WS
    ]

    let NonEvalWP Wasteprocessors with [WPBIEvaluated? = false]
    Ask NonEvalWP
    [
      WPEvaluate                                                           ;; Flowchart Z: WPEvaluate
    ]
  ]

  set countWS count Wastesuppliers
  set countWP count Wasteprocessors

  if CheckFailure?
  [
    output-print (word "countWS:" countWS " countWP:" countWP)
  ]

  ifelse countWS != 0 OR countWP != 0
  [
    ask Wastesuppliers
      [
        WSUpdateLinks                                                         ;; Flowchart S: WSUpdateLinks
    ]
  ]
  [
    ask patch 0 0 [set plabel "Symbiotic Network Failed"]
    set SNFailure True
    if CheckFailure?
    [
      output-print (word "SNFailure:" SNFailure)
    ]

  ]


  if CheckProduct? [
    ask Wasteprocessors[
    output-print (word "NewCurrentCBIDs" CurrentCBIDsList )
    output-print (word "NewPastCBIDs" HistoricalCBIDsList )
    ]
  ]
  update-plots
  if CheckFailure?
    [
      output-print (word "plot" )
  ]

  ;ForBehaviorSpace

  tick

  if CheckFailure?
    [
      output-print (word "tick" ticks)
  ]


end

;-----------------------------------------------------------------------------------------------------------------------------------------------;

to go                                                                      ;; FLOWCHART 03


  if CheckPath? [
    output-print "03 Go"
  ]

  go-once

  ;let CountWP count Wasteprocessors
  ;let CountWS count Wastesuppliers

  ForBehaviorSpace

  if ticks >= 40 ;OR CountWP = 0
  [ stop ]                                                  ;; stop after x seasons

end

;-----------------------------------------------------------------------------------------------------------------------------------------------;
To default_settings                                                         ;; FLOWCHART 04
                                                                            ;;SCENARIOS
  ;set TPB? false

  ;WS
  set initWSPBC 0.5
  set initWSWFPBC 1
  set initWSEPBC 0.04
  set initWSAtt 0.5
  set initWSWFatt 1
  set initWSEAtt 0.04
  set initWSSN 0.5
  set initWSWFSN 1
  set initWSESN 0.04

  set WSRNorm 0.2
  set WSStepOutB 0.35
  set WSEBFailure 0.3

  ;WP
  set initWPPBC 0.5
  set initWPWFPBC 1
  set initWPEPBC 0.04
  set initWPAtt 0.5
  set initWPWFAtt 1
  set initWPEAtt 0.04
  set initWPSN 0.5
  set initWPWFSN 1
  set initWPESN 0.04

  set WPRNorm 0.2
  set WPStepOutB 0.35
  set WPEBFailure 0.3

  ;WI
  set WSWIMaxfixedContractLength 0

  ;market
  set CPrice 28
  set BPPrice 0
  set WPrice 40

  ;negotiations
  set MaxOfferCounter 5

  ;;INTERVENTIONS
  ;quality control
  set WSWQualRNorm 0.3
  set WPQualThresholdPerc 0.5
  set WSQualPenalty? true

  ;quantity control
  set WPMinProcThresholdPerc 0.1
  set WPProcThresholdPerc 0.95

  ;stepping out of ISN
  set WSStepOutMoney -5000
  set WPStepOutMoney -10000
  set WPStepOutEmpty 2

  ;waste label
  set Waste_as_by-product? false

  ;technology
  set I/Oratio 0.5
  set ProcCostsperTon 33

  ; design parameters
  set MaxQuantityAllowed 480

  ;contract
  set Contract_Length 1

  ;sharing costs and revenue
  set PercAvoidedWasteCosts2WS 1
  set PercProductYield2WP 1


end

;-----------------------------------------------------------------------------------------------------------------------------------------------;
                                                                                                                    ;; Flowchart AJ: ForBehaviorSpace
to ForBehaviorSpace                                                                                                  ; create Global Variables for easiers export in BehaviorSpace (avoid NA values)

  if CheckPath?
  [
    output-print "AJ ForBehaviorSpace"
  ]

  let CountWP count Wasteprocessors
  let CountWS count Wastesuppliers

  ;print [WSID] of Wastesuppliers

  foreach WastesupplierIDList [                                                                                      ; make sure no NA values are exported in Behaviorspace
    Y ->
    let myWS one-of Wastesuppliers with [WSID = Y]

    if myWS != nobody [

      WSMakeGlobal
                 myWS
                 Y
    ]
  ]

  let myWP Wasteprocessor 16
  if myWP != nobody [
    WPMakeGlobal
                 myWP
  ]

  ifelse CountWP = 0 OR CountWS = 0 [
    if CheckBSpace? [
      output-print (word "MeanContracts 0")
    ]
    set MeanContractPrices 0
  ]
  [
    if CheckBSpace? [
      output-print (word "MeanContracts " mean [ContractAgreedPrice] of Contracts )
    ]

    set MeanContractPrices mean ([ContractAgreedPrice] of Contracts)
  ]

end

;-----------------------------------------------------------------------------------------------------------------------------------------------;

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;                OLD PROCEDURES
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

;-----------------------------------------------------------------------------------------------------------------------------------------------;


;to Export_Results_to_CSV
;set-current-directory "C:/Users/kphllange/surfdrive/Shared/promotie/werkmap/Re-organise/ABM/TvW composthub/bruikbaar"

;  let d-a-t (remove-item 6(remove-item 7(remove-item 8 (remove "-"(remove " "(remove "." (remove ":" date-and-time)))))))

;  export-plot "Yield and avoided costs per supplier" (word "YieldandAvoidCosts" d-a-t ".csv")
;  export-plot "Wastesupplier final offers per bidding round" (word "Valueaccept" d-a-t ".csv")
;  export-plot "Available waste per supplier" (word "WSSeason" d-a-t ".csv")
;  export-plot "BI" (word "BehavioralIntention" d-a-t ".csv")
;  export-plot "Waste in processor (tons)" (word "WasteInProcessor" d-a-t ".csv")
;  export-plot "Value of waste per supplier" (word "Wvalue" d-a-t ".csv")

;  export-interface (word "Interface" d-a-t ".png")

;  print "plots exported"

;end

;-----------------------------------------------------------------------------------;
@#$#@#$#@
GRAPHICS-WINDOW
701
453
1090
843
-1
-1
9.3
1
10
1
1
1
0
1
1
1
-20
20
-20
20
0
0
1
ticks
30.0

BUTTON
368
14
462
48
NIL
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

PLOT
1058
15
1440
178
WP accepted offers
Season
WSOffer
0.0
40.0
0.0
60.0
true
true
"let ContractList [ContractID] of Contracts\nask Wastesuppliers [pen-up]\nforeach ContractList\n[Y -> \n\n\nlet my-contract one-of Contracts with [ContractID = Y]\nlet my-WS one-of wastesuppliers with [WSID = [WSPartnerID] of my-contract]\nask my-WS [ pen-up]\n\nask my-WS [ pen-down] \nplotxy ticks [ContractAgreedPrice] of my-contract\nask my-WS [ pen-up]\n]" "let ContractList [ContractID] of Contracts\nask Wastesuppliers [pen-up]\nforeach ContractList\n[Y -> \n\n\nlet my-contract one-of Contracts with [ContractID = Y]\nlet my-WS one-of wastesuppliers with [WSID = [WSPartnerID] of my-contract]\nask my-WS [ pen-up]\n\nask my-WS [ pen-down] \nplotxy ticks [ContractAgreedPrice] of my-contract\nask my-WS [ pen-up]\n]"
PENS
"pen-0" 1.0 0 -7500403 true "" ""

SLIDER
7
151
106
184
initWSPBC
initWSPBC
0
1
0.5
0.1
1
NIL
HORIZONTAL

PLOT
1448
15
1827
178
Total cash flow per agent
Seasons
Euro
0.0
40.0
0.0
10.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks WSmoney\n  ]]\n \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color color\n  plotxy ticks WPmoney\n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks WSmoney\n  ]]\n\nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPmoney\n  ]]"
PENS

SLIDER
109
151
226
184
initWSWFPBC
initWSWFPBC
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
7
191
226
224
initWSEPBC
initWSEPBC
0
0.2
0.0
0.02
1
NIL
HORIZONTAL

SLIDER
5
231
104
264
initWSAtt
initWSAtt
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
107
231
225
264
initWSWFatt
initWSWFatt
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
6
269
225
302
initWSEAtt
initWSEAtt
0
0.2
0.0
0.02
1
NIL
HORIZONTAL

SLIDER
106
309
225
342
initWSWFSN
initWSWFSN
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
6
309
104
342
initWSSN
initWSSN
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
6
347
225
380
initWSESN
initWSESN
0
0.2
0.0
0.02
1
NIL
HORIZONTAL

PLOT
1512
192
1827
450
BI
NIL
NIL
0.0
40.0
0.0
1.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSBI\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPBI\n  ]]\n" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSBI\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPBI\n  ]]\n  "
PENS

INPUTBOX
621
340
680
410
WPrice
47.09
1
0
Number

INPUTBOX
494
340
549
410
CPrice
15.77
1
0
Number

SLIDER
254
151
359
184
initWPPBC
initWPPBC
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
364
151
476
184
initWPWFPBC
initWPWFPBC
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
254
191
477
224
initWPEPBC
initWPEPBC
0
0.2
0.0
0.02
1
NIL
HORIZONTAL

SLIDER
253
231
356
264
initWPAtt
initWPAtt
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
359
231
476
264
initWPWFAtt
initWPWFAtt
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
253
269
478
302
initWPEAtt
initWPEAtt
0
0.2
0.0
0.02
1
NIL
HORIZONTAL

SLIDER
486
632
672
665
Contract_Length
Contract_Length
1
16
1.0
1
1
NIL
HORIZONTAL

PLOT
1094
594
1432
719
Value of waste or by-product per Supplier
NIL
NIL
0.0
40.0
0.0
2.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  If Waste_as_by-product? = False [plotxy ticks WSWvalue]\n  If Waste_as_by-product? = True [plotxy ticks WSBPValue]\n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  If Waste_as_by-product? = False [plotxy ticks WSWvalue]\n  If Waste_as_by-product? = True [plotxy ticks WSBPValue]\n  ]]"
PENS

PLOT
702
15
1052
180
Waste in processor (tons)
NIL
NIL
0.0
40.0
0.0
480.0
true
false
"" ""
PENS
"default" 0.2 0 -16777216 true "foreach sort-on [who] Wasteprocessors\n\n[the-Wasteprocessor -> ask the-Wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\" who)\n  set-plot-pen-color color\n  ;let my-WPID self\n  ;let my-CBs CBs with [OwnedbyWPID = my-WPID]\n  ;let SumCBMass sum [CBMass] of my-CBs  \n  plotxy ticks WPProc\n  ]]" "foreach sort-on [who] Wasteprocessors\n\n[the-Wasteprocessor -> ask the-Wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\" who)\n  set-plot-pen-color color\n  ;let my-WPID self\n  ;let my-CBs CBs with [OwnedbyWPID = my-WPID]\n  ;let SumCBMass sum [CBMass] of my-CBs  \n  plotxy ticks WPProc\n  ]]"

SLIDER
485
714
671
747
PercAvoidedWasteCosts2WS
PercAvoidedWasteCosts2WS
0
1
1.0
0.1
1
NIL
HORIZONTAL

SLIDER
485
754
672
787
PercProductYield2WP
PercProductYield2WP
0
1
1.0
0.1
1
NIL
HORIZONTAL

INPUTBOX
553
339
618
409
BPPrice
0.0
1
0
Number

SWITCH
10
812
192
845
Waste_as_by-product?
Waste_as_by-product?
1
1
-1000

INPUTBOX
514
811
569
874
I/Oratio
0.5
1
0
Number

SLIDER
253
309
359
342
initWPSN
initWPSN
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
363
309
478
342
initWPWFSN
initWPWFSN
0
7
0.0
1
1
NIL
HORIZONTAL

SLIDER
253
349
478
382
initWPESN
initWPESN
0
2
0.0
0.02
1
NIL
HORIZONTAL

BUTTON
483
14
570
48
NIL
go-once\n
NIL
1
T
OBSERVER
NIL
O
NIL
NIL
1

BUTTON
587
14
690
49
NIL
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

PLOT
1094
454
1433
590
Wquality
NIL
NIL
0.0
40.0
0.0
1.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks [WSWqual] of the-wastesupplier\n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks [WSWqual] of the-wastesupplier\n  ]]"
PENS

PLOT
702
192
974
448
PBC
NIL
NIL
0.0
40.0
0.0
1.0
true
false
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSPBC\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPPBC\n  ]]\n" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSPBC\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPPBC\n  ]]\n"
PENS

PLOT
1245
193
1511
449
SN
NIL
NIL
0.0
40.0
0.0
1.0
true
false
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSSN\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPSN\n  ]]\n" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSSN\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPSN\n  ]]\n"
PENS

PLOT
973
192
1245
448
Att
NIL
NIL
0.0
40.0
0.0
1.0
true
false
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSAtt\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPAtt\n  ]]\n" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSAtt\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPAtt\n  ]]\n"
PENS

SLIDER
253
390
478
423
WPRNorm
WPRNorm
0
0.5
0.0
0.05
1
NIL
HORIZONTAL

SLIDER
7
390
226
423
WSRNorm
WSRNorm
0
0.5
0.0
0.05
1
NIL
HORIZONTAL

SLIDER
354
630
473
663
WPProcThresholdPerc
WPProcThresholdPerc
0
1
0.95
0.01
1
NIL
HORIZONTAL

SLIDER
8
669
233
702
WPQualThresholdPerc
WPQualThresholdPerc
0
1
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
6
432
225
465
WSStepOutB
WSStepOutB
0
1
0.0
0.05
1
NIL
HORIZONTAL

SLIDER
253
434
478
467
WPStepOutB
WPStepOutB
0
1
0.0
0.05
1
NIL
HORIZONTAL

SLIDER
8
629
233
662
WSWQualRNorm
WSWQualRNorm
0
0.5
0.3
0.1
1
NIL
HORIZONTAL

SLIDER
494
153
679
186
WSWIMaxfixedContractLength
WSWIMaxfixedContractLength
0
20
4.0
1
1
NIL
HORIZONTAL

SWITCH
1838
765
1969
798
CheckOffers?
CheckOffers?
1
1
-1000

SWITCH
2009
812
2139
845
CheckPath?
CheckPath?
1
1
-1000

INPUTBOX
487
543
584
603
MaxOfferCounter
5.0
1
0
Number

SWITCH
1973
765
2137
798
CheckTransactions?
CheckTransactions?
1
1
-1000

INPUTBOX
194
813
295
873
WSStepOutMoney
-1500.0
1
0
Number

INPUTBOX
299
813
400
873
WPStepOutMoney
-1500.0
1
0
Number

INPUTBOX
403
813
506
873
WPStepOutEmpty
2.0
1
0
Number

SWITCH
1838
812
2003
845
CheckFetch?
CheckFetch?
1
1
-1000

PLOT
1095
722
1431
843
WSWSeason
season
WSWSeason
0.0
40.0
0.0
2000.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks [WSWSeason] of the-wastesupplier\n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks [WSWSeason] of the-wastesupplier\n  ]]"
PENS

OUTPUT
1839
17
2748
757
11

PLOT
1437
454
1827
843
B
NIL
NIL
0.0
40.0
0.0
1.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSB\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPB\n  ]]\n" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word who \"Wastesupplier\")\n  set-plot-pen-color color\n  plotxy ticks WSB\n  ]]\n  \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPB\n  ]]\n"
PENS

TEXTBOX
20
130
208
153
Wastesupplier TPB variables
11
0.0
1

TEXTBOX
260
127
448
150
Wasteprocessor TPB variables\n
11
0.0
1

TEXTBOX
496
127
684
150
Existing contracts with incinerator
11
0.0
1

TEXTBOX
497
199
685
222
Contractual agreements incinerator
11
0.0
1

TEXTBOX
14
794
202
817
Residue label
11
0.0
1

TEXTBOX
517
791
653
819
Processing technology
11
0.0
1

TEXTBOX
497
302
685
330
Market prices (euro/tonne): \nCompost, By-product and Waste
11
0.0
1

SWITCH
10
81
114
114
TPB?
TPB?
1
1
-1000

TEXTBOX
488
611
703
637
Contractual agreements ISN
11
0.0
1

TEXTBOX
12
610
227
636
Quality control
11
0.0
1

TEXTBOX
256
609
471
635
Quantity control
11
0.0
1

INPUTBOX
249
671
352
731
MaxQuantityAllowed
480.0
1
0
Number

TEXTBOX
195
794
410
820
Leaving the network
11
0.0
1

TEXTBOX
490
524
587
550
Negotiation rounds
11
0.0
1

SWITCH
2146
813
2291
846
CheckContracts?
CheckContracts?
1
1
-1000

SLIDER
253
477
479
510
WPEBFailure
WPEBFailure
0
1
0.0
0.02
1
NIL
HORIZONTAL

SLIDER
8
477
227
510
WSEBFailure
WSEBFailure
0
1
0.0
0.02
1
NIL
HORIZONTAL

SWITCH
2146
765
2291
798
CheckFailure?
CheckFailure?
1
1
-1000

SLIDER
249
630
351
663
WPMinProcThresholdPerc
WPMinProcThresholdPerc
0
1
0.1
0.02
1
NIL
HORIZONTAL

BUTTON
213
15
338
50
NIL
Default_settings
NIL
1
T
OBSERVER
NIL
D
NIL
NIL
1

SWITCH
1844
848
1979
881
CheckBSpace?
CheckBSpace?
1
1
-1000

PLOT
698
858
1267
1201
Cumulative Transacted Waste from WS to WP or Composted Waste (WP to CB)
Seasons
Tonnes of transactions (cumulative)
0.0
40.0
0.0
10.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks CumuWS2WPTransact \n  ]]\n \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks CumuWP2CBTransact \n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks CumuWS2WPTransact \n  ]]\n \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks CumuWP2CBTransact \n  ]]"
PENS

PLOT
1279
858
1829
1201
MoneyPerUnit
Season
Euro per tonne waste
0.0
40.0
0.0
10.0
true
true
"foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks WSMoneyPerUnit\n  ]]\n \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPMoneyPerUnit \n  ]]" "foreach sort-on [who] Wastesuppliers\n\n[the-wastesupplier -> ask the-wastesupplier [\n  create-temporary-plot-pen (word \"Wastesupplier\" who)\n  set-plot-pen-color color\n  plotxy ticks WSMoneyPerUnit\n  ]]\n \nforeach sort-on [who] Wasteprocessors\n\n[the-wasteprocessor -> ask the-wasteprocessor [\n  create-temporary-plot-pen (word \"Wasteprocessor\")\n  set-plot-pen-color black\n  plotxy ticks WPMoneyPerUnit \n  ]]"
PENS

SWITCH
486
675
672
708
WSQualPenalty?
WSQualPenalty?
1
1
-1000

SWITCH
1989
849
2093
882
CheckB?
CheckB?
1
1
-1000

SWITCH
494
219
666
252
FlexMassWIContract?
FlexMassWIContract?
0
1
-1000

TEXTBOX
13
63
228
89
Actor behaviour variables
12
0.0
1

SWITCH
2149
852
2282
885
CheckProduct?
CheckProduct?
1
1
-1000

INPUTBOX
574
811
672
874
ProcCostsPerTon
33.0
1
0
Number

TEXTBOX
12
525
200
548
Design variables
14
0.0
1

TEXTBOX
495
66
645
84
Context variables
12
0.0
1

@#$#@#$#@
## WHAT IS IT?

The model explains how behavioural intention influences local organic waste exchanges and compost production in Industrial Symbiotic Networks (ISN)

## HOW IT WORKS

More information can be found in this article and accompanying appendices: 
K.P.H. Lange, G. Korevaar, I. Nikolic and P. Herder (2020) Actor behaviour and economic robustness of Industrial Symbiosis Networks: an agent-based approach. 


## HOW TO USE IT

The model can only run if it includes all the additional NLS-files and the CSV-file.

Press Default_settings to read out the default settings.

Press Setup to read out data from the CSV-file and set up the ISN. 

Press go-once for running local waste exchanges for one season, or go for running 40 seasons (10 years)


## CREDITS AND REFERENCES

Developed by K.P.H. Lange et al. (TU Delft and AUAS)
https://www.tudelft.nl/tbm/over-de-faculteit/afdelingen/engineering-systems-and-services/people/phd-candidates/ir-kph-kasper-lange/

Data from HvA research project Re-Organise:
http://www.hva.nl/kc-techniek/gedeelde-content/projecten/projecten-algemeen/re-organise.html
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

factory
false
0
Rectangle -7500403 true true 76 194 285 270
Rectangle -7500403 true true 36 95 59 231
Rectangle -16777216 true false 90 210 270 240
Line -7500403 true 90 195 90 255
Line -7500403 true 120 195 120 255
Line -7500403 true 150 195 150 240
Line -7500403 true 180 195 180 255
Line -7500403 true 210 210 210 240
Line -7500403 true 240 210 240 240
Line -7500403 true 90 225 270 225
Circle -1 true false 37 73 32
Circle -1 true false 55 38 54
Circle -1 true false 96 21 42
Circle -1 true false 105 40 32
Circle -1 true false 129 19 42
Rectangle -7500403 true true 14 228 78 270

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

wormcomposter
true
0
Circle -2064490 true false 45 210 30
Circle -2064490 true false 60 225 30
Circle -2064490 true false 75 225 30
Circle -2064490 true false 90 210 30
Circle -2064490 true false 90 195 30
Circle -2064490 true false 90 180 30
Circle -2064490 true false 105 165 30
Circle -2064490 true false 120 165 30
Circle -2064490 true false 135 165 30
Circle -2064490 true false 150 150 30
Circle -2064490 true false 195 60 30
Circle -2064490 true false 180 45 30
Circle -2064490 true false 165 45 30
Circle -2064490 true false 150 45 30
Circle -2064490 true false 135 60 30
Circle -2064490 true false 120 75 30
Circle -2064490 true false 105 75 30
Circle -2064490 true false 195 75 30
Circle -2064490 true false 60 45 30
Circle -2064490 true false 75 45 30
Circle -2064490 true false 90 60 30
Circle -2064490 true false 210 180 30
Circle -2064490 true false 210 195 30
Circle -2064490 true false 195 210 30
Circle -2064490 true false 180 210 30
Circle -2064490 true false 165 225 30
Circle -2064490 true false 150 225 30
Circle -2064490 true false 195 165 30
Circle -6459832 false false 0 0 300

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="Exp2ef TPB 2x last tick Exp2" repetitions="2" sequentialRunOrder="false" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp 1_1 Baseline NoTPB 0.5 200x all ticks exp1" repetitions="200" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_1 Baseline NoTPB 0.5 100x all ticks exp1" repetitions="100" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_4 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.45WS0.25" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_3 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.25WS0.45" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_7 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.35WS0.25" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_6 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.25WS0.35" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_8 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.35WS0.35" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp 2d TPB 1x last tick Exp2" repetitions="1" sequentialRunOrder="false" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.2"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_2TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.25WS0.25" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp1_5 TPB 0.5 10x all ticks exp1 stepoutmoney stepout WP0.45WS0.45" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp2abc Baseline NoTPB 0.5 2000x last tick exp2" repetitions="2000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp 2d TPB 500x last tick Exp2Rev1" repetitions="500" sequentialRunOrder="false" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp 1_2 TPB 500x allticksRev1" repetitions="500" sequentialRunOrder="false" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0.25"/>
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0.25"/>
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Exp 1_1 Baseline NoTPB 0.5 2000x all ticks exp1Rev1" repetitions="2000" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>SNFailure = true</exitCondition>
    <metric>PeriodID</metric>
    <metric>SNFailure</metric>
    <metric>initWSB1</metric>
    <metric>initWSB2</metric>
    <metric>initWSB3</metric>
    <metric>initWSB4</metric>
    <metric>initWSB5</metric>
    <metric>initWSB6</metric>
    <metric>initWSB7</metric>
    <metric>initWSB8</metric>
    <metric>initWSB9</metric>
    <metric>initWSB10</metric>
    <metric>initWSB11</metric>
    <metric>initWSB12</metric>
    <metric>initWSB13</metric>
    <metric>initWSB14</metric>
    <metric>initWSB15</metric>
    <metric>initWSB16</metric>
    <metric>WSB1</metric>
    <metric>WSB2</metric>
    <metric>WSB3</metric>
    <metric>WSB4</metric>
    <metric>WSB5</metric>
    <metric>WSB6</metric>
    <metric>WSB7</metric>
    <metric>WSB8</metric>
    <metric>WSB9</metric>
    <metric>WSB10</metric>
    <metric>WSB11</metric>
    <metric>WSB12</metric>
    <metric>WSB13</metric>
    <metric>WSB14</metric>
    <metric>WSB15</metric>
    <metric>WSB16</metric>
    <metric>WSMoneyPerUnit1</metric>
    <metric>WSMoneyPerUnit2</metric>
    <metric>WSMoneyPerUnit3</metric>
    <metric>WSMoneyPerUnit4</metric>
    <metric>WSMoneyPerUnit5</metric>
    <metric>WSMoneyPerUnit6</metric>
    <metric>WSMoneyPerUnit7</metric>
    <metric>WSMoneyPerUnit8</metric>
    <metric>WSMoneyPerUnit9</metric>
    <metric>WSMoneyPerUnit10</metric>
    <metric>WSMoneyPerUnit11</metric>
    <metric>WSMoneyPerUnit12</metric>
    <metric>WSMoneyPerUnit13</metric>
    <metric>WSMoneyPerUnit14</metric>
    <metric>WSMoneyPerUnit15</metric>
    <metric>WSMoneyPerUnit16</metric>
    <metric>CumuWS2WPTransact1</metric>
    <metric>CumuWS2WPTransact2</metric>
    <metric>CumuWS2WPTransact3</metric>
    <metric>CumuWS2WPTransact4</metric>
    <metric>CumuWS2WPTransact5</metric>
    <metric>CumuWS2WPTransact6</metric>
    <metric>CumuWS2WPTransact7</metric>
    <metric>CumuWS2WPTransact8</metric>
    <metric>CumuWS2WPTransact9</metric>
    <metric>CumuWS2WPTransact10</metric>
    <metric>CumuWS2WPTransact11</metric>
    <metric>CumuWS2WPTransact12</metric>
    <metric>CumuWS2WPTransact13</metric>
    <metric>CumuWS2WPTransact14</metric>
    <metric>CumuWS2WPTransact15</metric>
    <metric>CumuWS2WPTransact16</metric>
    <metric>initWPB0</metric>
    <metric>WPB0</metric>
    <metric>WPMoneyPerUnit0</metric>
    <metric>CumuWP2CBTransact0</metric>
    <metric>MeanContractPrices</metric>
    <metric>Count Contracts</metric>
    <metric>Count Wastesuppliers</metric>
    <metric>Count Wasteprocessors</metric>
    <metric>WSMoney1</metric>
    <metric>WSMoney2</metric>
    <metric>WSMoney3</metric>
    <metric>WSMoney4</metric>
    <metric>WSMoney5</metric>
    <metric>WSMoney6</metric>
    <metric>WSMoney7</metric>
    <metric>WSMoney8</metric>
    <metric>WSMoney9</metric>
    <metric>WSMoney10</metric>
    <metric>WSMoney11</metric>
    <metric>WSMoney12</metric>
    <metric>WSMoney13</metric>
    <metric>WSMoney14</metric>
    <metric>WSMoney15</metric>
    <metric>WSMoney16</metric>
    <metric>WPMoney0</metric>
    <enumeratedValueSet variable="TPB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFatt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWSESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPPBC">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEPBC">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPAtt">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPEAtt">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPSN">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPWFSN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initWPESN">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPRNorm">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutB">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPEBFailure">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWIMaxfixedContractLength">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CPrice">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="BPPrice">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPrice">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxOfferCounter">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSWQualRNorm">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPQualThresholdPerc">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSQualPenalty?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="FlexMassWIContract?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPMinProcThresholdPerc">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPProcThresholdPerc">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WSStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutMoney">
      <value value="-1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="WPStepOutEmpty">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Waste_as_by-product?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="I/Oratio">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="MaxQuantityAllowed">
      <value value="480"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Contract_Length">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercAvoidedWasteCosts2WS">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PercProductYield2WP">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckOffers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckTransactions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFailure?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckFetch?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckPath?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckContracts?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckBSpace?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckB?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="CheckProduct?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ProcCostsPerTon">
      <value value="33"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
