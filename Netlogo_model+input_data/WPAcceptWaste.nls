; WP Procedure
; Flowchart W: WPAcceptWaste

to WPAcceptWaste
  [
    WS-2-WP-Transact
    my-contract
  ]
  
   if CheckPath? [
    output-print "W WPAcceptWaste"
  ]
  ;print my-contract

   
let my-WP self
  

  If CheckTransactions?
  [
    output-print (word [WPID] of my-WP "WS2WP"WS-2-WP-Transact "myContract" my-contract)
  ]
    
;print temp-best-supplier-info
  let WAgreedTransactionPrice [ContractAgreedPrice] of my-contract
  let my-ContractAgreedMass [ContractAgreedMass] of my-contract
  ;print WAgreedTransactionPrice
  let my-WS-ID [WSPartnerID] of my-contract
  let my-WS one-of wastesuppliers with [WSID = my-WS-ID]
  let my-WS-W-Qual [WSWQual] of my-WS
 
  let WP2CBtransact 0
  let WP2WItransact 0
  
  ask my-WP
  [
    
    let WIAgent (min-one-of Incinerators [distance myself])                                   ; define Incinerator
    
    ifelse my-WS-W-Qual < WPQualThreshold                                                    ;; If the quality is too low: WP accepts waste according to contract, but gives WS a penalty to pay for the dumpint costs
    [
      set WP2CBtransact 0
      Set WP2WItransact WS-2-WP-Transact
      
      set WPOldMoney WPMoney 
      set WPMoney 
      (
        WPMoney + 
        (WS-2-WP-Transact * WAgreedTransactionPrice ) -                                        ; increment money for WS's payment of transacting waste to WP
        (WP2WItransact * [WIprice] of WIagent)                                                 ; decrement money for all mandatory (contracted) accepted waste that is can't be processed because quality is too low
      ) 
      set WPNewMoney WPMoney  
 
      
      If CheckTransactions?
      [
        output-print (word "QualityTooLow!" "WP2CB" WP2CBtransact "WP2WI" WP2WItransact)
      ]
    ]
    
    [                                                                                        ;; if the quality is OK, then WP accepts the waste according to contract. But if WP is full, he has to dump the abundant waste and pay the dumping costs himself.
      let ExpectedWPProc WPProc + WS-2-WP-Transact                                              ; CHECK WHETHER WP CAPACITY WILL NOT BE EXCEEDED AFTER TRANSACTION
      ;print ExpectedWPProc
      let ExpectedWPMaxCapLeft WPMaxProc - ExpectedWPProc
      let ActualWPMaxCapLeft [WPMaxCapLeft] of my-WP
      ;print ExpectedWPMaxCapLeft
    
      set WP2CBtransact WS-2-WP-Transact
      Set WP2WItransact 0
    
      ifelse ExpectedWPMaxCapLeft <= 0                                                          ; DEFENSIVE PROGRAMMING TO AVOID TOO MUCH INPUT IN THE PROCESSOR
      [
        set WP2CBtransact ActualWPMaxCapLeft 
        set WP2WItransact WS-2-WP-Transact - ActualWPMaxCapLeft
      
        If CheckTransactions?
        [
          output-print (word "QualityOK!" "CBAlmostFull" "WP2CB" WP2CBtransact "WP2WI" WP2WItransact)
        ]
      ]
      
      [
        set WP2CBtransact WS-2-WP-Transact  
        set WP2WItransact 0    
        
        If CheckTransactions?
        [
          output-print (word "QualityOK!" "AllinCB" "WP2CB" WP2CBtransact "WP2WI" WP2WItransact)
        ]
      ]
      
      set CumuWP2CBTransact CumuWP2CBTransact + WP2CBtransact                                        ; calculate cumulatives
      ;show CumuWP2CBTransact
      
      set WPOldMoney WPMoney                                                                         ; Remember what the amount of money was BEFORE transaction. This is necessary for WSEvaluate
    
      ifelse Waste_as_by-product? = false
      [                                                                                             ;; Waste as waste
        
        ifelse FlexMassWIContract? = false [
          set WPMoney                                                                                  ; ASSUMPTION: all agreed mass is accepted by WP and paid for by WS
          (WPMoney + 
          (WS-2-WP-Transact * WAgreedTransactionPrice ) -                                            ; increment money for WS's payment of transacting waste to WP
          (WS-2-WP-Transact * WAgreedTransactionPrice * I/ORatio * (1 - PercProductYield2WP))        ; decrement money in case some of the profit of compostproduction (according to contract) from this waste is shared with the original WS
          ;+ (WS-2-WP-Transact * WPrice * (1 - PercAvoidedWasteCosts2WS))                            ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: increment money in case some of the avoided dumping costs (according to contract) are shared with WP by the original WS
          ;- (WP2WItransact * [WIprice] of WIagent)                                                  ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: decrement money for all mandatory (contracted) accepted waste that is can't be processed because WP is full 
          )
        ]
        [
          set WPMoney                                                                                  ; ASSUMPTION: all agreed mass is accepted by WP and paid for by WS
          (WPMoney + 
          (WS-2-WP-Transact * WAgreedTransactionPrice ) -                                            ; increment money for WS's payment of transacting waste to WP
          (WS-2-WP-Transact * WAgreedTransactionPrice * I/ORatio * (1 - PercProductYield2WP))        ; decrement money in case some of the profit of compostproduction (according to contract) from this waste is shared with the original WS
          + (WS-2-WP-Transact * WPrice * (1 - PercAvoidedWasteCosts2WS))                             ; increment money in case some of the avoided dumping costs (according to contract) are shared with WP by the original WS
          - (WP2WItransact * [WIprice] of WIagent)                                                   ; decrement money for all mandatory (contracted) accepted waste that is can't be processed because WP is full 
          )
        ]
        
      ]
      
      [                                                                                             ;; Waste as byproduct
        
        ifelse FlexMassWIContract? = false [
          set WPMoney                                                                                  ; ASSUMPTION: all agreed mass is accepted by WP and paid for by WP
          (
            WPMoney - 
            (WS-2-WP-Transact * WAgreedTransactionPrice ) -                                            ; decrement money for buying 'resource' waste-as-by-product from WS
            (WS-2-WP-Transact * WAgreedTransactionPrice * I/ORatio * (1 - PercProductYield2WP))        ; decrement money in case some of the profit of future production of compost of this waste is shared with the original WS 
            ;+ (WS-2-WP-Transact * WPrice * (1 - PercAvoidedWasteCosts2WS))                            ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: increment money in case some of the avoided dumping costs are shared with WP by the original WS 
            ;- (WP2WItransact * [WIprice] of WIagent)                                                  ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: decrement money for all mandatory (contracted) accepted waste that is can't be processed because WP is full 
          )
        ]
        [
          set WPMoney                                                                                  ; ASSUMPTION: all agreed mass is accepted by WP and paid for by WP
          (
            WPMoney - 
            (WS-2-WP-Transact * WAgreedTransactionPrice ) -                                            ; decrement money for buying 'resource' waste-as-by-product from WS
            (WS-2-WP-Transact * WAgreedTransactionPrice * I/ORatio * (1 - PercProductYield2WP))        ; decrement money in case some of the profit of future production of compost of this waste is shared with the original WS 
            + (WS-2-WP-Transact * WPrice * (1 - PercAvoidedWasteCosts2WS))                             ; increment money in case some of the avoided dumping costs are shared with WP by the original WS 
            - (WP2WItransact * [WIprice] of WIagent)                                                   ; decrement money for all mandatory (contracted) accepted waste that is can't be processed because WP is full 
          )
        ]
      ]
    
      ifelse CumuWP2CBTransact != 0 [                                               
        set WPMoneyPerUnit WPMoney / CumuWP2CBTransact                                               ; [euro per ton waste], calculate WSMoneyPerUnit (cumulative over time)
      ]                                ;
      [
        set WPMoneyPerUnit 0
      ]

      set WPNewMoney WPMoney                                                                         ; Remember what the amount of money was AFTER transaction. This is necessary for WSEvaluate
    
    
      WPFillCB                                                                                      ;; FLOWCHART AB: waste is put in compost batch
               WP2CBtransact
    
                                                                                                    ;; FLOWCHART Q: WP acts as a wastesupplier for WI here, so we go to flowchart Q
    ]
    
    if WP2WItransact > 0 [
      let my-WSlist4 WSWPDumpWaste                                                                  ;; Flowchart Q: report transaction of dumped waste
                        my-WP                                                                        ; inputs from Wastesuppliers
                        WP2WItransact
                        
                        WIAgent                                                                      ; inputs from Incinerator
                        [WIAmountIncinerated] of WIagent
                        [WIprice] of WIagent
                        [WIMoney] of WIagent

                                                                                                    ;; output dump waste
      set WP2WItransact item 0 my-WSlist4                                                               ; Wastesupplier variables
                                                                                                        ;set [WIAmountIncinerated] of WIagent item 1 my-WSlist4
                                                                                                        ;set [WIMoney] of WIagent item 2 my-WSlist4
    ]
  ]
 
   
  
end
