; WS procedure
; Flowchart P


to-report WSTransact                                                 ;; Wastesupplier procedure
  [
    my-contract
  ]
  
  let my-WS self
  
   if CheckPath? [
    output-print (word "P WSTransact" " my-WSID:" [WSID] of my-WS)
  ]
 
;  let my-contract one-of Contracts with [WSPartnerID = [WSID] of my-WS]
  let my-WPID [WPPartnerID] of my-contract 
  let my-WP one-of Wasteprocessors with [WPID = my-WPID]
  let myAgreedPrice [ContractAgreedPrice] of my-contract
  let my-AgreedMass [ContractAgreedMass] of my-contract
  let my-WI min-one-of Incinerators [distance my-WS]
  let old-WSWSeason [WSWSeason] of my-WS
  ;print my-contract
  
  ifelse my-WP != nobody
  [
    let my-WPQualThreshold [WPQualThreshold] of my-WP    
    
    ask my-WP
    [
    let my-CBs CBs with [OwnedbyWPID = my-WPID]
    set WPProc sum [CBMass] of my-CBs
    set WPMaxCapLeft WPMaxProc - WPProc
    ]
    
    let my-CBs CBs with [OwnedbyWPID = my-WPID]
    let MaxCapLeft [WPMaxCapLeft] of my-WP
    let MaxProc [WPMaxProc] of my-WP
    let Proc [WPProc] of my-WP
   
   ;print MaxCapLeft
   ;print my-WP
  
  ask my-WS                                                           ;; transact waste from Wastesuppliers to Wasteprocessor
    [
      
      ifelse (my-AgreedMass > WSWSeason)
            
      [
      ifelse (WSWSeason < MaxCapLeft)
      [
        set WS2WPTransact WSWSeason                                       ; define amount of transacted waste
        set WS2WITransact WSWSeason - WSWSeason
      ]
      [
        set WS2WPTransact MaxCapLeft
        set WS2WITransact WSWSeason - MaxCapLeft
      ]
      ]
      
      [
      ifelse (my-AgreedMass < MaxCapLeft)
      [
        set WS2WPTransact my-AgreedMass                                       ; define amount of transacted waste
        set WS2WITransact WSWSeason - my-AgreedMass
      ]
      [
        set WS2WPTransact MaxCapLeft
        set WS2WITransact WSWSeason - MaxCapLeft
      ]
      ]
      
      set WSWSeason WSWSeason - WS2WPTransact                                        ; ask Contractsupplier to set waste to 0
      
      set CumuWS2WPTransact CumuWS2WPTransact + WS2WPTransact                        ; calculate cumulatives
      
      set WSOldMoney WSMoney                                                         ; Remember what the amount of money was BEFORE transaction. This is necessary for WSEvaluate
      
      ifelse Waste_as_by-product? = false [
        ifelse FlexMassWIContract? = false [
          set WSMoney 
          (
            WSMoney  - 
            (WS2WPTransact * myAgreedPrice) +                                           ; decrement money for costs of transacting waste
            (WS2WPTransact * myAgreedPrice * I/ORatio * (1 - PercProductYield2WP))      ; increment money in case part of the yield from the production of compost is shared with the original wastesupplier
            ;+ (WS2WPTransact * Wprice * PercAvoidedWasteCosts2WS)                      ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: increment money for avoided dumping costs
          )  
        ]
        [
          set WSMoney 
          (
            WSMoney  - 
            (WS2WPTransact * myAgreedPrice) +                                           ; decrement money for costs of transacting waste
            (WS2WPTransact * myAgreedPrice * I/ORatio * (1 - PercProductYield2WP))      ; increment money in case part of the yield from the production of compost is shared with the original wastesupplier
            + (WS2WPTransact * Wprice * PercAvoidedWasteCosts2WS)                       ; increment money for avoided dumping costs
          )  
        ] 
      ]
      [
        ifelse FlexMassWIContract? = false [
          set WSMoney 
          (
            WSMoney  + 
            (WS2WPTransact * myAgreedPrice ) +                                         ; increment money for selling waste to processor
            (WS2WPTransact * myAgreedPrice * I/ORatio * (1 - PercProductYield2WP))     ; increment money in case part of the yield from the production of compost is shared with the original wastesupplier
            ;+ (WS2WPTransact * Wprice * PercAvoidedWasteCosts2WS)                     ; NOT APPLICABLE FOR TRADITIONAL WI CONTRACTS: increment money for avoided dumping costs
          )  
        ]
        [
          set WSMoney 
          (
            WSMoney  + 
            (WS2WPTransact * myAgreedPrice ) +                                         ; increment money for selling waste to processor
            (WS2WPTransact * myAgreedPrice * I/ORatio * (1 - PercProductYield2WP))     ; increment money in case part of the yield from the production of compost is shared with the original wastesupplier
            + (WS2WPTransact * Wprice * PercAvoidedWasteCosts2WS)                      ; increment money for avoided dumping costs
          )
        ]
      ]
      
      if WSQualPenalty? = True [                                                       ; if the penalty intervention is turned on
      
        if WSWQual < my-WPQualThreshold                                             ;; THis is the penalty WS gets from WP if the quality is lower than the contract requires
        [
            set WSMoney (WSMoney - (WS2WPTransact * [WIprice] of my-WI))             ; my-WS has to pay for WP's dump costs, because its WSs fault that the quality is to low for processing
            ;                                                                        ; the initially accepted waste is forwarded to WI to be dumped, see flowchart W. WPAcceptWaste
            
        ]
      ]
      
      ifelse CumuWS2WPTransact != 0 [                                               
        set WSMoneyPerUnit WSMoney / CumuWS2WPTransact                             ; [euro per ton waste], calculate WSMoneyPerUnit (cumulative over time)
      ]                                ;
      [
        set WSMoneyPerUnit 0
      ]

      set WSNewMoney WSMoney                                                         ; Remember what the amount of money was AFTER transaction. This is necessary for WSEvaluate
      
      set label precision WSWSeason 1
        
      ]
    
  ]
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                                                                   ;; DEFENSIVE PROGRAMMING:
  [
  ask my-WS                                                                         ; if there is no local WP, transact only to incinerator. ADD TO FLOWCHART!
    [
      set WS2WPTransact 0
      set WS2WITransact WSWSeason
    ]
  ]
    
  ;update-plots
  
  If CheckTransactions?
  [
    output-print (word [WSID] of my-WS "WS2WP" WS2WPTransact "WS2WI" WS2WITransact)
 
    let TransactControl ( (WS2WPTransact + WS2WITransact) - old-WSWSeason )
    let TransactOK? True
    if TransactControl != 0 [set TransactOK? False]
        
    output-print (word "Check Balance = 0? " TransactControl TransactOK?)
   
  ]
  
    
 report 
        (list                                                                      ;;report  
          WS2WITransact
          WS2WPTransact
        )  
 

end


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
; Auxiliary procedures
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

