#'goede' code
options(stringsAsFactors = FALSE)

library("ggplot2", lib.loc="~/R/win-library/3.6")
library("reshape2", lib.loc="~/R/win-library/3.6")
library("gsubfn", lib.loc="~/R/win-library/3.6")
library("proto", lib.loc="~/R/win-library/3.6")
library("RSQLite", lib.loc="~/R/win-library/3.6")
library("sqldf", lib.loc="~/R/win-library/3.6")
library(dplyr)
library(tidyr)
library(viridis)
library(matrixStats)

# Set the working directory
setwd(".../Rcode+Exp_data/Exp2")

###############################################################
#########Calculate amount of failures exp1#####################
###############################################################

#########
#make dataframes from data
Exp2a = read.table("20201011 Exp2abc NoTPB 2000x lasttickRev1", skip = 6, sep = ",", dec = ".", head=TRUE, stringsAsFactors = FALSE)
Exp2b = read.table("20201008 Exp2d TPB 200x lasttick rev1", skip = 6, sep = ",", dec = ".", head=TRUE, stringsAsFactors = FALSE)

DF = rbind(Exp2a, Exp2b)

MedianinitWSB <- rowMedians(as.matrix(DF[,60:75]))
MininitWSB <- rowMins(as.matrix(DF[,60:75]))
MaxinitWSB <- rowMaxs(as.matrix(DF[,60:75]))

DF$MedianinitWSB <- MedianinitWSB
DF$MininitWSB <- MininitWSB
DF$MaxinitWSB <- MaxinitWSB

#Rename columns
colnames = colnames(DF)
colnames(DF) = gsub("X\\.", "", colnames(DF))
colnames(DF) = gsub("^\\.|\\.", "", colnames(DF))
colnames(DF) = gsub("\\[|\\]", "", colnames(DF))
colnames(DF) = gsub("\\.", "_", colnames(DF))
colnames(DF) = gsub("__", "_", colnames(DF))
colnames(DF) = gsub("ofWastesupplier", "", colnames(DF))
colnames(DF) = gsub("ofWasteprocessor", "", colnames(DF))
colnames(DF) = gsub("ofContracts","", colnames(DF))
colnames(DF)[1] = "Run_Number"  

#Subsetting
Exp2_1NoTPB = subset(DF, TPB == "false")
Exp2_2TPB = subset(DF, TPB == "true")

Exp2_2aWS0_0to0_2 = subset(Exp2_2TPB, MedianinitWSB>=0 & MedianinitWSB<0.2 | MininitWSB>=0 & MininitWSB<0.2 | MaxinitWSB>=0 & MaxinitWSB<0.2)
Exp2_2aWS0_2to0_4 = subset(Exp2_2TPB, MedianinitWSB>=0.2 & MedianinitWSB<0.4 | MininitWSB>=0.2 & MininitWSB<0.4 | MaxinitWSB>=0.2 & MaxinitWSB<0.4)
Exp2_2bWS0_4to0_6 = subset(Exp2_2TPB, MedianinitWSB>=0.4 & MedianinitWSB<0.6 | MininitWSB>=0.4 & MininitWSB<0.6 | MaxinitWSB>=0.4 & MaxinitWSB<0.6)
Exp2_2cWS0_6to0_8 = subset(Exp2_2TPB, MedianinitWSB>=0.6 & MedianinitWSB<0.8 | MininitWSB>=0.6 & MininitWSB<0.8| MaxinitWSB>=0.6 & MaxinitWSB<0.8)
Exp2_2cWS0_8to1_0 = subset(Exp2_2TPB, MedianinitWSB>=0.8 & MedianinitWSB<=1 | MininitWSB>=0.8 & MininitWSB<=1 | MaxinitWSB>=0.8 & MaxinitWSB<=1)

Exp2_2dWP0_0to0_2 = subset(Exp2_2TPB, initWPB0>=0 & initWPB0<0.2)
Exp2_2dWP0_2to0_4 = subset(Exp2_2TPB, initWPB0>=0.2 & initWPB0<0.4)
Exp2_2eWP0_4to0_6 = subset(Exp2_2TPB, initWPB0>=0.4 & initWPB0<0.6)
Exp2_2fWP0_6to0_8 = subset(Exp2_2TPB, initWPB0>=0.6 & initWPB0<0.8)
Exp2_2fWP0_8to1_0 = subset(Exp2_2TPB, initWPB0>=0.8 & initWPB0<=1)

#Count number of objects in subsetted dataframes
MaxRunNumbersexp2_1 = nrow(Exp2_1NoTPB)

MaxRunNumbersexp2_2a = nrow(Exp2_2aWS0_0to0_2)
MaxRunNumbersexp2_2b = nrow(Exp2_2aWS0_2to0_4)
MaxRunNumbersexp2_2c = nrow(Exp2_2bWS0_4to0_6)
MaxRunNumbersexp2_2d = nrow(Exp2_2cWS0_6to0_8)
MaxRunNumbersexp2_2e = nrow(Exp2_2cWS0_8to1_0)

MaxRunNumbersexp2_2f = nrow(Exp2_2dWP0_0to0_2)
MaxRunNumbersexp2_2g = nrow(Exp2_2dWP0_2to0_4)
MaxRunNumbersexp2_2h = nrow(Exp2_2eWP0_4to0_6)
MaxRunNumbersexp2_2i = nrow(Exp2_2fWP0_6to0_8)
MaxRunNumbersexp2_2j = nrow(Exp2_2fWP0_8to1_0)

#Count number of SNFailure = false
SNSuccessexp2_1 = sum(c(Exp2_1NoTPB$SNFailure == "false"))

SNSuccessexp2_2a = sum(c(Exp2_2aWS0_0to0_2$SNFailure == "false"))
SNSuccessexp2_2b = sum(c(Exp2_2aWS0_2to0_4$SNFailure == "false"))
SNSuccessexp2_2c = sum(c(Exp2_2bWS0_4to0_6$SNFailure == "false"))
SNSuccessexp2_2d = sum(c(Exp2_2cWS0_6to0_8$SNFailure == "false"))
SNSuccessexp2_2e = sum(c(Exp2_2cWS0_8to1_0$SNFailure == "false"))

SNSuccessexp2_2f = sum(c(Exp2_2dWP0_0to0_2$SNFailure == "false"))
SNSuccessexp2_2g = sum(c(Exp2_2dWP0_2to0_4$SNFailure == "false"))
SNSuccessexp2_2h = sum(c(Exp2_2eWP0_4to0_6$SNFailure == "false"))
SNSuccessexp2_2i = sum(c(Exp2_2fWP0_6to0_8$SNFailure == "false"))
SNSuccessexp2_2j = sum(c(Exp2_2fWP0_8to1_0$SNFailure == "false"))

#Calculate percentages of success
SNSuccessexp2_1Perc = 100 * SNSuccessexp2_1 / MaxRunNumbersexp2_1

SNSuccessexp2_2aPerc = 100 * SNSuccessexp2_2a / MaxRunNumbersexp2_2a
SNSuccessexp2_2bPerc = 100 * SNSuccessexp2_2b / MaxRunNumbersexp2_2b
SNSuccessexp2_2cPerc = 100 * SNSuccessexp2_2c / MaxRunNumbersexp2_2c
SNSuccessexp2_2dPerc = 100 * SNSuccessexp2_2d / MaxRunNumbersexp2_2d
SNSuccessexp2_2ePerc = 100 * SNSuccessexp2_2e / MaxRunNumbersexp2_2e

SNSuccessexp2_2fPerc = 100 * SNSuccessexp2_2f / MaxRunNumbersexp2_2f
SNSuccessexp2_2gPerc = 100 * SNSuccessexp2_2g / MaxRunNumbersexp2_2g
SNSuccessexp2_2hPerc = 100 * SNSuccessexp2_2h / MaxRunNumbersexp2_2h
SNSuccessexp2_2iPerc = 100 * SNSuccessexp2_2i / MaxRunNumbersexp2_2i
SNSuccessexp2_2jPerc = 100 * SNSuccessexp2_2j / MaxRunNumbersexp2_2j

#Create dataframe for plot
#DFforPlot <- data.frame(NamesDF = c("Baseline: TPB OFF", "initWSB: 0-0.2", "initWSB: 0.2-0.4", "initWSB: 0.4-0.6", "initWSB: 0.6-0.8", "initWSB: 0.8-1", "initWPB: 0.2-0.4", "initWPB: 0.4-0.6", "initWPB: 0.6-0.8"),
DFforPlot <- data.frame(NamesDF = c("N/A", "initWSB: 0-0.2", "initWSB: 0.2-0.4", "initWSB: 0.4-0.6", "initWSB: 0.6-0.8", "initWSB: 0.8-1","initWPB: 0-0.2","initWPB: 0.2-0.4", "initWPB: 0.4-0.6", "initWPB: 0.6-0.8","initWPB: 0.8-1"),
#                        SNSuccesDF= c(SNSuccessexp2_1Perc, SNSuccessexp2_2aPerc, SNSuccessexp2_2bPerc, SNSuccessexp2_2cPerc, SNSuccessexp2_2dPerc, SNSuccessexp2_2ePerc, SNSuccessexp2_2fPerc),
                        SNSuccesDF= c(SNSuccessexp2_1Perc, SNSuccessexp2_2aPerc, SNSuccessexp2_2bPerc, SNSuccessexp2_2cPerc, SNSuccessexp2_2dPerc, SNSuccessexp2_2ePerc, SNSuccessexp2_2fPerc, SNSuccessexp2_2gPerc, SNSuccessexp2_2hPerc, SNSuccessexp2_2iPerc, SNSuccessexp2_2jPerc),
#                        Scenario= c("N/A","WSB","WSB","WSB","WPB","WPB","WPB"))
                        Variable= c("TPB OFF","TPB ON: initWSB","TPB ON: initWSB","TPB ON: initWSB","TPB ON: initWSB","TPB ON: initWSB","TPB ON: initWPB","TPB ON: initWPB","TPB ON: initWPB","TPB ON: initWPB","TPB ON: initWPB"))

DFforPlot$SNSuccesDF <- round(DFforPlot$SNSuccesDF, digits = 0)
DFforPlot$NamesDF <- factor(DFforPlot$NamesDF, levels = c("N/A","initWPB: 0-0.2","initWPB: 0.2-0.4", "initWPB: 0.4-0.6", "initWPB: 0.6-0.8","initWPB: 0.8-1", "initWSB: 0-0.2", "initWSB: 0.2-0.4", "initWSB: 0.4-0.6", "initWSB: 0.6-0.8", "initWSB: 0.8-1"))
#DFforPlot&Variable <- factor(DFforPlot$Variable, levels = c("No TPB","TPB initWSB","TPB initWSB","TPB initWSB","TPB initWSB","TPB initWSB","TPB initWPB","TPB initWPB","TPB initWPB","TPB initWPB","TPB initWPB"))
#Plot percentage of successful ISNs over initWSB and initWPB
Exp2BehaviourSuccessratePlot <- ggplot(DFforPlot, aes(x = DFforPlot$NamesDF, y = DFforPlot$SNSuccesDF, levels=DFforPlot$NamesDF, colour=Variable, fill=Variable)) + 
  geom_bar(stat = "identity", width=0.4) +
  geom_text(aes(label=SNSuccesDF), vjust=-0.5, position = position_dodge(0.9), size=4.5, colour = "black")+
  scale_colour_manual ("", values = c("TPB OFF" = "grey", "TPB ON: initWSB" = "black", "TPB ON: initWPB" = "black")) + 
  scale_fill_manual ("", values = c("TPB OFF" = "white", "TPB ON: initWSB" = "grey90", "TPB ON: initWPB" = "grey40")) + 
  theme_bw()+ 
  theme(text = element_text(size=15), axis.text.x = element_text(angle = 45, hjust = 1), legend.position="top")+
  ylim (0,100) +
  labs (x = "Initial behaviour values", y = "Percentage of runs with surviving ISNs [%]") #+
#  ggtitle("Successrate ISNs compared to initial behaviour values of processor and suppliers (n=500)")
print(Exp2BehaviourSuccessratePlot)

#ggsave(Exp2BehaviourSuccessratePlot, file="20201013 Exp2 Successrate ISNs per init behaviour.png",
#width = 6.7, height = 6, dpi = 300, device='png')

